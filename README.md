# `zmq-wss`
One-way bridge from ØMQ `PUB` socket to in-browser `WebSocket`.

# Motivation
This kind of bridge makes sense if there is a backend system (here represented
by the
[`zmq-pub`](https://gitlab.com/bmcorser/zmq-wss/blob/master/docker-compose.yml#L7)
container) that wants to send messages to an in-browser client (eg.
[`src/client.html`](src/client.html)). ØMQ is a nice messaging system, but
isn’t _a priori_ compatible with the WebSocket protocol. Why not crudely bolt
them together? The repository demonstrates how one might do just that.


# Infra
![Diagram](https://docs.google.com/drawings/d/e/2PACX-1vRQ0upskg3tZaDDMhSQ6rIOxVaai4ttAYWW2GkS-YM4KhWQT-tySwOicGfjnd0Kuz4X-x-MZpOqlxfq/pub?w=1440&h=1080)
