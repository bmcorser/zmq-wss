extern crate websocket;
extern crate zmq;

use std::thread;
use websocket::sync::Server;
use websocket::OwnedMessage;

fn main() {
  let ws_server = Server::bind("0.0.0.0:80").unwrap();

  for request in ws_server.filter_map(Result::ok) {
    // Spawn a new thread for each connection.
    thread::spawn(move || {
      if !request.protocols().contains(&"rust-websocket".to_string()) {
        request.reject().unwrap();
        return;
      }

      let client = request.use_protocol("rust-websocket").accept().unwrap();
      let ip = client.peer_addr().unwrap();
      println!("Connection from {}", ip);

      let (_, mut sender) = client.split().unwrap();

      let zmq_ctx = zmq::Context::new();
      let subscriber = zmq_ctx.socket(zmq::SUB).unwrap();
      assert!(subscriber.connect("tcp://zmq-pub:5556").is_ok());
      println!("Connected to zmq PUB socket");
      assert!(subscriber.set_subscribe(b"").is_ok());

      let mut zmq_msg = zmq::Message::new().unwrap();
      loop {
        if subscriber.recv(&mut zmq_msg, zmq::DONTWAIT).is_err() {
          continue;
        }
        let ws_message = OwnedMessage::Text(
          zmq_msg.as_str().unwrap().to_string()
        );
        sender.send_message(&ws_message).unwrap();
      }
      
    });
  }
}
