#include "zhelpers.h"

int main (int argc, char *argv [])
{
    //  Socket to talk to server
    printf ("Collecting updates from weather server…\n");
    void *context = zmq_ctx_new ();
    void *subscriber = zmq_socket (context, ZMQ_SUB);
    printf("zmq_socket: %s\n", strerror(errno));
    int rc = zmq_connect (subscriber, "tcp://zmq-pub:5556");
    printf("zmq_connect: %s\n", strerror(errno));
    assert (rc == 0);

    rc = zmq_setsockopt (subscriber, ZMQ_SUBSCRIBE, "", 0);
    assert (rc == 0);

    while (1) {
      zmq_msg_t zmq_msg;
      zmq_msg_init (&zmq_msg);
      int rc = zmq_msg_recv (&zmq_msg, subscriber, ZMQ_DONTWAIT);
      if (rc < 0 && errno == EAGAIN) {
        continue;
      } else {
        printf("made it\n");
        break;
      }
    }
    zmq_close (subscriber);
    zmq_ctx_destroy (context);
    return 0;
}
