//  Weather update server
//  Binds PUB socket to tcp://*:5556
//  Publishes random weather updates

#include <unistd.h>
#include "zhelpers.h"

int main (void)
{
    //  Prepare our context and publisher
    void *context = zmq_ctx_new ();
    printf("zmq_ctx_new: %s\n", strerror(errno));
    void *publisher = zmq_socket (context, ZMQ_PUB);
    printf("zmq_socket: %s\n", strerror(errno));
    int rc = zmq_bind (publisher, "tcp://*:5556");
    assert (rc == 0);
    printf("zmq_bind: %s\n", strerror(errno));

    //  Initialize random number generator
    srandom ((unsigned) time (NULL));
    while (1) {
        //  Get values that will fool the boss
        int zipcode, temperature, relhumidity;
        zipcode     = randof (100000);
        temperature = randof (215) - 80;
        relhumidity = randof (50) + 10;

        //  Send message to all subscribers
        char update [20];
        sprintf (update, "%05d %d %d", zipcode, temperature, relhumidity);
        s_send (publisher, update);
        sleep(1);
    }
    zmq_close (publisher);
    zmq_ctx_destroy (context);
    return 0;
}
